# Template for your own mod with Visual Studio Code

## Download
[Template](https://gitlab.com/Synchrony-Mods/modtemplate/uploads/084a17d154779dc1521205736978899f/ModTemplate.zip)

## Required
- Visual Studio Code
- Extension: Lua (sumneko.lua)

## Initial setup for new mods
1. Download the ZIP and extract the content into your desired mod folder inside of '%localappdata%\Synchrony\Mods'
2. Open workspace in VS-Code via 'YourModName/.vscode/workspace.code-workspace'
3. Enter your mod data into the 'mod.json'
4. Start Synchrony to check, if the mod loads correctly

## Initial setup for existing mod
1. Unzip the folders **.vscode**, **build**, **lib** and **utils** to your mod folder.
2. Open workspace in VS-Code via 'YourModName/.vscode/workspace.code-workspace' to check, if workspace is setup correctly

## Add your own Mod to the auto-completion
1. Run **utils/LinkLib.bat** with admin rights
2. Enter 'this' on the promt
3. The folder 'build' should now contain a symlink to the scripts folder named after your mod

## Add other mods to the auto-completion
1. Run **utils/LinkLib.bat** with admin rights
2. Enter the name of the mod on the promt (source files must be placed in Synchrony/Mods)
3. The folder 'lib' should now contain a symlink to the scripts folder named after the mod
4. VS-Code now shows the mod under 'YourMod/lib/OtherMod'